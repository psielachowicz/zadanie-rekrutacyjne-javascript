import { BoardSize } from './interfaces';

export const boardSize: BoardSize = {
    x: 5,
    y: 5
}

export const gameTime = 60;

export const initialLifes = 3;
