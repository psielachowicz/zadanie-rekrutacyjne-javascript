export interface BoardSize {
    readonly x: number;
    readonly y: number;
}

export interface GameplayConfig {
    board?: BoardSize;
    lifes?: number;
    time?: number;
}