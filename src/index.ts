import { Gameplay } from './gameplay';
import { gameTime, initialLifes, boardSize } from './config';
import { GameplayConfig } from './interfaces';

const gameplayConfig: GameplayConfig = {
    time: gameTime,
    lifes: initialLifes,
    board: boardSize
}

const gameplay = new Gameplay(gameplayConfig);