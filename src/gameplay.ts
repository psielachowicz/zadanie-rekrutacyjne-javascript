import { BoardSize, GameplayConfig } from './interfaces';

export class Gameplay {
    public ctx: CanvasRenderingContext2D;
    public newX: number;
    public newY: number;
    public canvas: HTMLCanvasElement;
    public lifes: number;
    public timeLeft: number;
    public points = 0;
    public boardSize: BoardSize;
    public timeCounterInterval;
    public tileGeneratorInterval;
    public gameplayConfig: GameplayConfig;
    public gameState: 'play' | 'hold' = 'hold';

    constructor(gameplayConfig: GameplayConfig) {
        this.gameplayConfig = gameplayConfig;
        this.boardSize = gameplayConfig.board || { x: 5, y: 5 };
        this.setBoard();
        this.setActions();
    }

    setBoard(): void {
        this.canvas = document.getElementById("myCanvas") as HTMLCanvasElement;
        this.canvas.height = 55 * this.boardSize.y -13;
        this.canvas.width = 55 * this.boardSize.x - 13;
        this.ctx = this.canvas.getContext("2d");
        
        for(var x = 0; x < this.boardSize.x;x++) {
            for(var y = 0; y < this.boardSize.y;y++) {
                this.ctx.rect(55 * x + 1, 55 * y + 1, 40,40);
            }
        }

         this.ctx.strokeStyle = "black";
         this.ctx.stroke();
         this.ctx.fillStyle = "white";
         this.ctx.fill();
    }

    setInitialValues() {
        this.lifes = this.gameplayConfig.lifes || 3;
        this.timeLeft = this.gameplayConfig.time || 60;
        this.points = 0;

        document.querySelector("#points-indicator").innerHTML = this.points.toString();
        document.querySelector("#time-indicator").innerHTML = this.timeLeft.toString();
        document.querySelector("#life-indicator").innerHTML = this.lifes.toString();
    }

    setTimeCount() {
       this.timeCounterInterval = setInterval(() => {
            if(this.timeLeft > 0) {
                this.timeLeft--;
                document.querySelector("#time-indicator").innerHTML = this.timeLeft.toString();
            } else {
                this.gameOver();
            }
        },1000)
    }

    setTileGenerator() {
        this.tileGeneratorInterval = setInterval(() => {
            this.newX = Math.floor((Math.random() * this.boardSize.x));
            this.newY = Math.floor((Math.random() * this.boardSize.y));
            this.ctx.fillStyle = "green";
            this.ctx.fillRect(55 * this.newX + 1, 55 * this.newY + 1, 40,40);
            setTimeout(() => {
            this.ctx.rect(55 * this.newX + 1, 55 * this.newY + 1, 40,40);
            this.ctx.fillStyle = "white";
            this.ctx.fill();
            this.newX = null; 
            this.newY = null;
            }, 1000)
            }, 3000);
    }

    start() {
        this.setInitialValues();
        this.setTimeCount();
        this.setTileGenerator();
        this.gameState = 'play';
    }

    /**
     * Alert z podsumowaniem i zakończenie odliczania czasu na koniec gry
     * 
     */
    gameOver(): void {
        alert(`Koniec gry! zdobyte punkty: ${this.points}`);
        clearInterval(this.tileGeneratorInterval);
        clearInterval(this.timeCounterInterval);
        this.gameState = 'hold';
    }
    
    /**
     * Utrata życia
     * 
     */
    takeLife() {
        if(this.lifes > 1) {
            this.lifes--;
            document.querySelector("#life-indicator").innerHTML = this.lifes.toString();
            alert('Straciłeś życie!');
        } else {
            this.gameOver();
        }
    }

    addPoint(): void {
        this.points++;
        document.querySelector("#points-indicator").innerHTML = this.points.toString();
    }

    reset(): void {
        this.setInitialValues();
    }

    rightTileClicked(layerX: number,layerY: number): boolean {
        return layerX > 55 * this.newX && layerX < 55 * this.newX + 55
         && layerY > 55 * this.newY && layerY < 55 * this.newY + 55;
    }

    setActions(): void {
        document.addEventListener("click", (e: MouseEvent) => {
            let el = <HTMLElement>e.target;
            if (el.id === "start") {
                this.start();
            }

            if (el.id === "reset") {
                this.reset();
            }
        });

        this.canvas.addEventListener('click', event => {

            if(this.gameState === 'hold') {
                return;
            }

            if(this.rightTileClicked(event.layerX, event.layerY)) {
            this.addPoint();
            } else {
            this.takeLife();
            }
            })
    }
}